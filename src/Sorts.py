######################################################################################################################
# Name: Brennan Forrest
# Date: 12/15/17
# Description: Implements various sorting algorithms and reveals how many swaps and comparisons take place
######################################################################################################################

# creates the list
def getList():
	return [100, 5, 63, 29, 69, 74, 96, 80, 82, 12]
#	return [82, 65, 93, 0, 60, 31, 99, 90, 31, 70]
#	return [63, 16, 78, 69, 36, 36, 3, 66, 75, 100]
#	return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#	return [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
#	return [2, 1, 4, 3, 6, 5, 8, 7, 10, 9]

# the bubble sort function
# input: a list of integers
# output: a number of comparisons and swaps
def bubble_sort(list):
	comparisons = 0
	swaps = 0
	n = len(list) - 1
 
	while n >= 0: #Essentially begin counting from the back of the list
		for i in range(0, n):
			comparisons += 1
			if list[i] > list[i + 1]: #Keep swapping neighboring numbers if the one on the left is larger than the one on the right
				temp = list[i]
				list[i] = list[i + 1]
				list[i + 1] = temp
				swaps += 1 #And also count them

		n -= 1

	return comparisons, swaps



# the optimized bubble sort function
# input: a list of integers
# output: a number of comparisons and swaps
def optimized_bubble_sort(list):
	comparisons = 0
	swaps = 0
	swaps_this_pass = 0
	n = len(list) - 1

	#Do the exact same thing as the other bubble sort function
	while n >= 0:
		for i in range(0, n):
			comparisons += 1
			if list[i] > list[i + 1]:
				temp = list[i]
				list[i] = list[i + 1]
				list[i + 1] = temp
				swaps += 1
				swaps_this_pass += 1 #Except it counts how many swaps have been performed in every pass

		#If it's ever 0, break because the list is sorted
		if swaps_this_pass == 0:
			break

		n -= 1
		swaps_this_pass = 0

	return comparisons, swaps


# the selection sort function
# input: a list of integers
# output: a number of comparisons and swaps
def selection_sort(list):
	comparisons = 0
	swaps = 0
	n = len(list) #Don't have to subtract 1 since the range functions do that already

	for i in range(0, n): #Begin iterating through the list 
		for j in range(i, n): #Begin iterating through the sorted portion of the list
			comparisons += 1
			if (list[i] > list[j]): #Check if the item on the left is greater than the item on the right
				swaps += 1
				list[i], list[j] = list[j], list[i] #I'm able to use this syntax since I have access to i and j

	return comparisons, swaps

# the insertion sort function
# input: a list of integers
# output: a number of comparisons and swaps
def insertion_sort(list):
	comparisons = 0
	swaps = 0


	for i in range(1, len(list)):
		current_value = list[i]
		index = i #Copy i because it will be updated in an inner loop

		#This and the statement find what index the unsorted item should be inserted at
		while index > 0 and list[index - 1] > current_value:
			list[index] = list[index - 1]
			swaps += 1
			comparisons += 1
			index -= 1 

		list[index] = current_value

	return comparisons, swaps

# the main part of the program

bubble_sort_list = getList()
optimized_bubble_sort_list = getList()
selection_sort_list = getList()
insertion_sort_list = getList()

print("The list: {}".format(getList()))
comparisons, swaps = bubble_sort(bubble_sort_list)
print("After bubble sort: {}".format(bubble_sort_list))
print("{} comparisons; {} swaps".format(comparisons, swaps))

print("The list: {}".format(getList()))
comparisons, swaps = optimized_bubble_sort(optimized_bubble_sort_list)
print("After optimized bubble sort: {}".format(optimized_bubble_sort_list))
print("{} comparisons; {} swaps".format(comparisons, swaps))

print("The list: {}".format(getList()))
comparisons, swaps = selection_sort(selection_sort_list)
print("After selection sort: {}".format(selection_sort_list))
print("{} comparisons; {} swaps".format(comparisons, swaps))

print("The list: {}".format(getList()))
comparisons, swaps = insertion_sort(insertion_sort_list)
print("After insertion sort: {}".format(insertion_sort_list))
print("{} comparisons; {} swaps".format(comparisons, swaps))
