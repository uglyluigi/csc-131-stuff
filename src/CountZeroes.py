from math import log10

#################################################
#Name: Brennan Forrest
#Date: 12/12/17
#Description: Counts the number of zeroes you would have to write when writing all number from 1-MAGIC_NUMBER
#################################################


################ CONSTANTS ################
MAGIC_NUMBER = 1000000

################ FUNCTIONS ################

#Counts how many zeroes you'd have to write if you wrote out every number
#from 1 to x. Does it the lame way though.
#O(n).
def count_em_lame(x):
	zeroes = 0

	for i in range(1, x + 1): #Take every number from 1 to x 
		zeroes += str(i).count('0') #Convert the number to a string, count the number of zeroes in the string, and add it to the total zeroes

	return zeroes

#Counts how many zeroes you'd have to write if you wrote out
#every number from 1 to x. Does it the cool mathmatical way.
#This took me 30x as long as the string implementation and
#is 3x as slow. But it's unique and it saves valuable RAM for 
#us Chrome users.
def count_em_ballin(x):
	zeroes = 0

	for i in range(1, x + 1): #Begins looping from 1 to x (upper bound on range is exclusive so add 1); the main loop
		for j in range(0, int(log10(i))): #Start looping through from 0 to whatever the max number of zeroes the number could have. Exclusive doesn't matter here
			if (i // 10 ** j) % 10 == 0: #Divide each number from 1 to x by 10^j and then see if the remainder is zero when divided by 10. If it is that means
				zeroes += 1				#the number at the (10^j)-s place is a zero and adds it. 

	return zeroes


################ MAIN PART OF PROGRAM #########################

print count_em_lame(MAGIC_NUMBER)
print count_em_ballin(MAGIC_NUMBER)