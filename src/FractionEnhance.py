######################################################################################################################
# Name: Brennan Forrest
# Date: 1/5/18
# Description: A class that represents a fraction 
######################################################################################################################

# the fraction class
class Fraction(object):

	#The fraction class.
	#Defaults to 0/1, denominators that = 0 will be set to 1 automatically.
	def __init__(self, num = 0, den = 1):
		self._num = num
		
		if den == 0:
			den = 1
		
		self._den = den
		self.reduce()

	@property
	def num(self):
		return self._num

	@property
	def den(self):
		return self._den

	@num.setter
	def num(self, num):
		self._num = num
		self.reduce()

	@den.setter
	def den(self, den):
		if den == 0:
			self._den = 1
		else:
			self._den = den

		self.reduce()

	def gcf(self):
		gcf = max(self.num, self.den)

		#Counts down from the max gcf value to 1 and tries to divide the numerator and denominator evenly.
		#Returned if it does. Returns 1 if the loop reaches 1 without finding it.
		while gcf > 1:
			if self.num % gcf == 0 and self.den % gcf == 0:
				return gcf

			gcf -= 1

		return 1

	def reduce(self):
		gcf = self.gcf()
		
		if gcf > 1:
			self._num /= gcf
			self._den /= gcf

		#Handling for negative numbers
		if self.den < 0: #If the negative sign is on the bottom, move it to the top.
			self._num *= -1
			self._den *= -1
		elif self.num < 0 and self.den < 0: #If both num and den are negative, reduce it to a postitive fraction.
			self._num = abs(self._num)
			self._den = abs(self._den)

		return self

	def __str__(self):
		return "{}/{} ({})".format(self.num, self.den, float(self.num) / self.den)

	#Function that makes addition and subtraction overloads appear less redundant by returning a tuple of the things those functions need
	def arith_help(self, fraction_2):
		return self.den * fraction_2.den, self.num * fraction_2.den, fraction_2.num * self.den

	def __add__(self, fraction_2):
		new_denominator, newmerator_1, newmerator_2 = self.arith_help(fraction_2)
		return Fraction(newmerator_1 + newmerator_2, new_denominator).reduce()

	def __sub__(self, fraction_2):
		new_denominator, newmerator_1, newmerator_2 = self.arith_help(fraction_2)
		return Fraction(newmerator_1 - newmerator_2, new_denominator).reduce()

	def __mul__(self, fraction_2):
		return Fraction(self.num * fraction_2.num, self.den * fraction_2.den).reduce()

	def __div__(self, fraction_2):
		reciprocal = Fraction(fraction_2.den, fraction_2.num)
		return (self * reciprocal).reduce()

# ***DO NOT MODIFY OR REMOVE ANYTHING BELOW THIS POINT!***
# the main part of the program
# create some fractions
f1 = Fraction()
f2 = Fraction(5, 8)
f3 = Fraction(3, 4)
f4 = Fraction(1, 0)

# display them
print "f1:", f1
print "f2:", f2
print "f3:", f3
print "f4:", f4

# play around
f3.num = 5
f3.den = 8
f1 = f2 + f3
f4.den = 88
f2 = f1 - f1
f3 = f1 * f1
f4 = f4 / f3

# display them again
print
print "f1:", f1
print "f2:", f2
print "f3:", f3
print "f4:", f4

